import logo from './logo.svg'
import './App.css'

import React, { useEffect, useState } from 'react'
import {
  initiateSocket,
  disconnectSocket,
  subscribeToChat,
  sendMessage
} from './Socket'
import { ModalMessage } from './components/ModalMessage'
import MensajeProvider from './context/message.context'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import { Home } from './components/Home'
function App () {
  const options = {
    // you can also just use 'bottom center'
    position: positions.BOTTOM_CENTER,
    timeout: 10000000,
    offset: '30px',
    // you can also just use 'scale'
    transition: transitions.SCALE
  }
  return (
    <>
      <AlertProvider template={AlertTemplate} {...options}>
        <MensajeProvider>
          <ModalMessage />
          <Home />
        </MensajeProvider>
      </AlertProvider>
    </>
  )
  // const emailsUsers1 = [
  //   'jdacosta@tecnomega.com',
  //   'irmasilva@tecnomega.com',
  //   'aarauz@tecnomega.com',
  //   'andreamartinez@tecnomega.com',

  // ]

  // const emailsUsers2 = [
  //   'johnsons@tecnomega.com',
  //   'andreamartinez@tecnomega.com',
  //   'jorgemarin@tecnomega.com',
  //   'aarauz@tecnomega.com'

  // ]
  // const [emailUser1, setEmailUser1] = useState(emailsUsers1[0])
  // const [emailUser2, setEmailUser2] = useState(emailsUsers2[0])
  // const [message, setMessage] = useState('')
  // const [chat, setChat] = useState([])

  // useEffect(() => {
  //   if (emailUser1) initiateSocket(emailUser1)
  //   subscribeToChat((err, data) => {
  //     console.log(data);
  //     // if (err) return
  //     setChat(data)
  //   })
  //   return () => {
  //     disconnectSocket()
  //   }
  // }, [emailUser1])

  // console.log(emailsUsers1)
  // console.log(chat)
  // return (
  //   <div>
  //     <h1>USER1: {emailUser1}</h1>
  //     {emailsUsers1.map((r, i) => (
  //       <button onClick={() => setEmailUser1(r)} key={i}>
  //         {r}
  //       </button>
  //     ))}
  //     <h1>USER2: {emailUser2}</h1>
  //     {emailsUsers2.map((r, i) => (
  //       <button onClick={() => setEmailUser2(r)} key={i}>
  //         {r}
  //       </button>
  //     ))}
  //     <h1>Live Chat:</h1>

  //     <input
  //       type='text'
  //       name='name'
  //       value={message}
  //       onChange={e => setMessage(e.target.value)}
  //     />
  //     <button onClick={() => sendMessage(emailUser1, emailUser2, message)}>
  //       Send
  //     </button>
  //     {chat.map((m, i) => (
  //       <p key={i}>{m.message}<small>  ==SENDER: {m.sender}</small> </p>
  //     ))}
  //   </div>

  // )
}

export default App
