import React, { useContext, Fragment } from 'react'
import { MensajeContext } from '../context/message.context'

export const Users = ({ users }) => {
  console.log(users)
  const { setrecept, setopenModalMensaje } = useContext(MensajeContext)
  const handleClickMensaje = user => {
    // setopenModalMensaje(true)
    setrecept(user.socketId)
    setopenModalMensaje(true)
  }
  return (
    <ul className='users-list'>
      {users.map(user => (
        <Fragment key={user.email}>
          <li
            key={user.email}
            className={user.socketId !== '' ? 'connect' : 'disconnect'}
            onClick={() => handleClickMensaje(user)}
          >
            {user.name}
            <br />
          </li>
          <p>{user.email}</p>
        </Fragment>
      ))}
    </ul>
  )
}
