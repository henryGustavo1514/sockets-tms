import React, { useContext, useState } from 'react'
import 'react-responsive-modal/styles.css'
import { Modal } from 'react-responsive-modal'
import { MensajeContext } from '../context/message.context'
import io from 'socket.io-client'

export const ModalMessage = () => {
  let socket = io.connect('http://172.16.3.94:4400')
  const [mensaje, setmensaje] = useState('')
  const {
    openModalMensaje,
    setopenModalMensaje,
    recept,
    userActive
  } = useContext(MensajeContext)

  const handleChangeMessage = e => {
    setmensaje(e.target.value)
  }
  const sendMessage = e => {
    e.preventDefault()
    console.log('object')
    let dataMessage = { receptor: recept, emisor: userActive, mensaje }
    console.log(dataMessage)
    // socket.emit('enviarMsgPrivado', data)
    socket.emit('enviarMsgPrivado', dataMessage)
  }
  return (
    <Modal
      open={openModalMensaje}
      onClose={() => {
        setopenModalMensaje(false)
      }}
      center
      closeOnOverlayClick={true}
      showCloseIcon={true}
      closeOnEsc={true}
      classNames={{
        overlay: 'customOverlay',
        modal: 'customModal'
      }}
    >
      <h1>Modal</h1>
      <form action='' onSubmit={sendMessage}>
        <input
          type='text'
          className='form-control'
          onChange={handleChangeMessage}
        />
        <button className='btn btn-info' type='submit'>
          Enviar
        </button>
      </form>
      {/* <ProductoContent /> */}
    </Modal>
  )
}
