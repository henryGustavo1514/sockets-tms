import React, { useContext, useState } from 'react'

import { Users } from './Users'
import io from 'socket.io-client'
import { MensajeContext } from '../context/message.context'
import { useAlert } from 'react-alert'

export const Home = () => {
  const { userActive, setuserActive } = useContext(MensajeContext)
  // const [userActive, setuserActive] = useState('')
  const alert = useAlert()
  const [users, setusers] = useState(null)

  const handleClickUserActive = async () => {
    try {
      let socket = io.connect('http://172.16.3.94:4400')
      await socket.emit('nuevoUser', userActive)
      // console.log(`Connecting socket...`)
      // if (socket && emailUser) {
      //   socket.emit('join', emailUser)
      // }
      await socket.on('clienteConectado', usersData => {
        console.log(usersData)
        setusers(usersData)
        // return cb(null, usersData)
        // users = usersData
        // getUsuarios()
      })
      socket.on('usuarioDesconectado', usersData => {
        setusers(usersData)
      })

      socket.on('recibirMensaje', data => {
        console.log('Mmensaje recibido', data)
        alert.show(`Mensaje de ${data.emisor} : 
        ${data.mensaje}
        `)
      })
      // console.log(users)
      // return users
    } catch (error) {
      console.log(error)
    }
  }
  const handleChange = e => {
    console.log(e.target.value)
    setuserActive(e.target.value)
  }

  return (
    <div className='container'>
      <div className='row'>
        <div className='col'>
          <h1>Home</h1>
          <input
            type='text'
            placeholder='Ingrese su nombre'
            className='form-control'
            value={userActive}
            onChange={handleChange}
          />
          <button className='btn btn-success' onClick={handleClickUserActive}>
            Set User Active
          </button>
        </div>
        {users && (
          <div className='col'>
            <Users users={users} />
          </div>
        )}
      </div>
    </div>
  )
}
