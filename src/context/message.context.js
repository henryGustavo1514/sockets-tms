import React, { createContext, useState } from 'react'

export const MensajeContext = createContext()

const MensajeProvider = props => {
  const [recept, setrecept] = useState(null)
  const [openModalMensaje, setopenModalMensaje] = useState(null)
  const [userActive, setuserActive] = useState('')

  return (
    <MensajeContext.Provider
      value={{
        recept,
        setrecept,
        openModalMensaje,
        setopenModalMensaje,
        userActive,
setuserActive
      }}
    >
      {props.children}
    </MensajeContext.Provider>
  )
}

export default MensajeProvider
